# postscriptum-find-process

## About

This is a fork of [find-process](https://github.com/yibn2008/find-process) without the binary and with reduced dependencies.

## License

[MIT](LICENSE)
